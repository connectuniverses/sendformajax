# SendFormAjax

Jquery plugin to send a form on Ajax.
it's possible to send it with a button or when an input change (the form is directly send when an input of the form change ).
Works with input type="file".

## Demo

You can found a demo [here](http://demo.connect-universes.com/sendformajax/)

## Instructions

You need to include [`jquery.js`](https://jquery.com/) first.

Form is automaticaly add if the data is correctly set (see below) when the page is loaded.

If a form is added after the page is loaded, you can add the form to the plugin with:

```
$("#my_form_after_loaded_page").sfa();
```

## Data mandatory

These data are mandatory on the form tag.

- action : contain the URL that receipt the form
- method : method use to send the form. Values available are:

=> POST/post

=> GET/get

- data-sfa_send :   contain if the form is send automaticaly or with a button. Values available are:

=> auto : The form is send as soon as the input change

=> button : The form is send with a button

- data-sfa_type : contain the type of the send/reponse. Values available are:

=> json

=> jsonp

=> xml

## Data optional

- OPTIONAL id :  the ID of the form
- OPTIONAL enctype  :  send the form with files. If there is no file, don't write this index. Value available is:

    => multipart/form-data

- OPTIONAL data-sfa_callback : Javascript Function callback when the response is correctly received. Function must received 1 argument, that is the response (xml / json) send by the server

```
callback_message = function(data){
    window.alert('Message from test.js -- callback_message function: Form is send');
}
```

- OPTIONAL data-sfa_callback_error: Javascript Function callback when the response is not correctly received. Function must received 2 arguments, first is the code status and the second is the text of the error return.

```
callback_error = function(status, textStatus){
    window.alert('Message from test.js -- callback_error function: Error call the form, check the console log: "'+status+'" "'+textStatus+'"');
}
```

- OPTIONAL data-sfa_lk_button : To disable button of the form (no multiple invoice) if the button is out the form.

    The button must have data-sfa_button attributes ([`See exemple`](http://demo.connect-universes.com/sendformajax/#send_form_btn_outside))

- OPTIONAL data-sfa_txtconfirm : a confirm javascript popup that is show before the send of the form. If the user don't confirm, the form is not send. Value is the texte of the popup
