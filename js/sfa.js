$(function()
{
    /*  REQUIRES :  $form is the reference of the form
        MODIFIES :  /
        EFFECTS :   If all is ok, send the Ajax.
                        if response is ok, callback fonction sfa_callback
                        else, callback funtion sfa_callback_error
    */
    doAjax_sfa = function($form){
        if(checkAttributeForm_sfa($form) == true){
            var confirm = true;
            if(typeof($form.attr('data-sfa_txtconfirm')) != "undefined"){
                if(!window.confirm($form.attr('data-sfa_txtconfirm'))){
                    confirm = false;
                }
            }

            if(confirm == true){
                var valueForm = $form.serializeArray();
                var contentType = "application/x-www-form-urlencoded; charset=UTF-8";
                var processData = true;
                var crossDomain = false;
                if($form.data('sfa_type') == "jsonp"){
                    crossDomain = true;
                }
                if(typeof($form.attr('enctype')) != "undefined" && $form.attr('enctype') == "multipart/form-data"){
                    var form = document.getElementById($form.attr('id'));
                    valueForm = new FormData(form);
                    contentType = false;
                    processData = false;
                }

                enabledDisabledForm_sfa($form, true);

                $.ajax({
                    type: $form.attr("method"),
                    url: $form.attr("action"),
                    data: valueForm,
                    dataType: $form.data("sfa_type"),
                    crossDomain: crossDomain,
                    processData: processData,
                    contentType: contentType,
                    success: function(data){
                        enabledDisabledForm_sfa($form, false);
                        if(typeof($form.attr('data-sfa_callback')) != "undefined"){
                            eval($form.data('sfa_callback')+'('+JSON.stringify(data)+')');
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        console.error(jqXHR.status + ' '+textStatus);
                        enabledDisabledForm_sfa($form, false);
                        if(typeof($form.attr('data-sfa_callback_error')) != "undefined"){
                            eval($form.data('sfa_callback_error')+'("'+jqXHR.status+'", "'+textStatus+'")');
                        }
                    }
                });
            }
        }
    };

    /* REQUIRES :   $form is the reference of the form
                    disabled is a boolean
                        => true: disabled Form
                        => false: enabled Form
       MODIFIES :   if sfa_send == auto, disable/enable all input/select/textarea of the form
                    if sfa_send == button, disable/enable all button of the form
       EFFECTS  :   /
    */
    function enabledDisabledForm_sfa($form, disabled){
        switch($form.data('sfa_send')){
            case 'auto':
                $form.find('input, select, textarea').prop('disabled', disabled);
            break;
            case 'button':
                $form.find('[type="submit"], [type="reset"]').prop('disabled', disabled);
                if(typeof($form.data('sfa_lk_button')) != "undefined"){
                    $('[data-sfa_button="'+$form.data('sfa_lk_button')+'"]').prop('disabled', disabled);
                }
            break;
        }
    }

    /*  REQUIRES :  options is an array
                    $this is the reference of the form. Ex: $('form#form')
                    attr is the index in the array options. Ex: options[attr]
        MODIFIES :  if options[attr] is undefined and $this.attr(attr) is not undefined, set options[attr] = $this.attr(attr)
        EFFECTS  :  return options
    */
    function checkOptionsForm_sfa(options, $this, attr){
        if(typeof(options[attr]) == "undefined"){
            if(typeof($this.attr(attr)) != "undefined"){
                options[attr] = $this.attr(attr);
            }
        }

        return options;
    };

    /*  REQUIRES :  $form is the reference of the form
        MODIFIES :  /
        EFFECTS  :  Check if the form has correct mandatory attribute.
                        return true if it's correct
                        return false if it's not correct and warn the incorrect attribute
    */
    function checkAttributeForm_sfa($form){
        var method = $form.attr('method');
        var data_sfa_send = $form.attr('data-sfa_send');
        var data_sfa_type = $form.attr('data-sfa_type');
        if(typeof($form.attr('action')) != "undefined"
        && typeof(method) != "undefined"
        && typeof(data_sfa_send) != "undefined"
        && typeof(data_sfa_type) != "undefined"){
            var error = false;
            if(method.toUpperCase() != "POST"
            && method.toUpperCase() != "GET"){
                console.warn("the form attr 'method' is not 'POST' OR 'GET'");
                error = true;
            }
            if(data_sfa_send != "auto"
            && data_sfa_send != "button"){
                console.warn("the form attr 'data-sfa_send' is not 'auto' OR 'button'");
                error = true;
            }
            if(data_sfa_type != "json"
            && data_sfa_type != "jsonp"
            && data_sfa_type != "xml"){
                console.warn("the form attr 'data-sfa_type' is not 'json' OR 'jsonp' OR 'xml'");
                error = true;
            }

            if(error == false){
                return true;
            }
            else{
                return false;
            }
        }
        else {
            console.warn("The mandatory attribute is not set correctly on the 'form'");
            return false;
        }
    }

    /*  REQUIRES :   options is an array with index
                        - action    :   contain the URL that receipt the form
                        - method    :   method use to send the form. Values available are:
                            => POST/post
                            => GET/get
                        - data-sfa_send :   contain if the form is send automaticaly or with a button. Values available are
                            => auto : The form is send as soon as the input change
                            => button : The form is send with a button
                        - data-sfa_type  :   contain the type of the send/reponse. Values available are
                            => json
                            => jsonp
                            => xml
                        - OPTIONAL id :  the ID of the form
                        - OPTIONAL enctype  :   send the form with files. If there is no file, don't write this index. Value available is
                            => multipart/form-data
                        - OPTIONAL data-sfa_callback : Function callback when the response is correctly received
                        - OPTIONAL data-sfa_callback_error: Function callback when the response is not correctly received
                        - OPTIONAL data-sfa_lk_button : To disable button of the form (no multiple invoice) if the button is out the form.
                            The button must have data-sfa_button attributes
                        - OPTIONAL data-sfa_txtconfirm   :   a confirm javascript popup that is show before the send of the form. If the user don't confirm, the form is not send. Value is the texte of the popup
        MODIFIES :    check if options index is defined
        EFFECTS  :    if sfa_send == auto, send form when all input/select/textarea change
                      else if sfa_send == button, send form when button is clicked

    */
    $.fn.sfa = function(options)
    {
        if(this.is('form')){
            if(typeof(options) == "undefined"){
                options = new Array();
            }

            // Check options index
            options = checkOptionsForm_sfa(options, this, "id");
            options = checkOptionsForm_sfa(options, this, "action");
            options = checkOptionsForm_sfa(options, this, "method");
            options = checkOptionsForm_sfa(options, this, "data-sfa_send");
            options = checkOptionsForm_sfa(options, this, "data-sfa_type");
            options = checkOptionsForm_sfa(options, this, "data-sfa_lk_button");
            options = checkOptionsForm_sfa(options, this, "enctype");
            options = checkOptionsForm_sfa(options, this, "data-sfa_txtconfirm");

            // Set options in the form attr
            $.each(options, function(index, value){
                this.attr(index, value);
            });

            if(checkAttributeForm_sfa(this) == true){
                if(options["data-sfa_send"] == "auto"){
                    this.unbind('submit')
                    .submit(function(){
                        return false;
                    });

                    var $form = this;
                    this.find('input, select, textarea')
                    .unbind('change')
                    .change(function(){
                        doAjax_sfa($form);
                        return false;
                    });
                }
                else {
                    this.unbind('submit')
                    .submit(function(){
                        doAjax_sfa($(this));
                        return false;
                    });
                }
            }
        }
        else {
            console.warn("The object passed is not a 'form'");
        }

        return this;
    };

    /* REQUIRES :   OPTIONAL exist in the page form[data-sfa_send]
       MODIFIES :   set sfa() on form[data-sfa_send]
       EFFECTS  :   /
    */
    if($('form[data-sfa_send]').length){
        $.each($('form[data-sfa_send]'), function(){
            $(this).sfa();
        });
    }
});
