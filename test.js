$(function()
{
    callback_message = function(data){
        window.alert('Message from test.js -- callback_message function: Form is send');
    }

    callback_error = function(status, textStatus){
        window.alert('Message from test.js -- callback_error function: Error call the form, check the console log: "'+status+'" "'+textStatus+'"');
    }

    // Send the form when click on button
    $('button[data-sfa_button="send_form_button_outside"]').click(function(){
        doAjax_sfa($('form[data-sfa_lk_button="send_form_button_outside"]'));
    });
});
